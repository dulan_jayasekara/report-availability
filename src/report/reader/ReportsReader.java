package report.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import report.model.ReportDetails;


public class ReportsReader {
	
	private ReportDetails reportsDetails;
	private StringBuffer  PrintWriter;
	private String currentDateforMatch;
	private int testCaseCount; 
	
	public ReportsReader(ReportDetails reportsdetails){
		this.reportsDetails=reportsdetails;
	}
	
	public void getReports() throws IOException{
		
		PrintWriter = new StringBuffer();
		testCaseCount = 1;
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 
		
		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Internal Reports Availability - Date ["+currentDateforMatch+"]</p></div>");
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");
		
		PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values</th><th>Actual Values</th><th>Test Status</th></tr>");
		
		PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>System Availability</td>");
		PrintWriter.append("<td>System Should be Available</td>");
				
		if (reportsDetails.isPageLoaded() == true) {
			
			PrintWriter.append("<td>System is Available</td>");
			PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
			
			testCaseCount ++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Login Status</td>");
			PrintWriter.append("<td>Should Login successfully</td>");
		
			if (reportsDetails.isLoginSuccess() == true) {
				
				PrintWriter.append("<td>Login is successful</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
				
				
				PrintWriter.append("<tr><td class='fontiiii'>Operations Reports</td><tr>"); 
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking List Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isBookingListReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking List Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isBookingListReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				///
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Confirmation Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isBookingConfirmReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Confirmation Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isBookingConfirmReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				///
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Quotation Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isQuotationReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Quotation Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isQuotationReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Airline Passenger Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAirPass_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Airline Passenger Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAirPass_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Auto Cancellation Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAutoCan_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Auto Cancellation Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAutoCan_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Modification Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isBookingMod_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Modification Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isBookingMod_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Cancellation Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCancelReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Cancellation Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCancelReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Chaser Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCusChaserReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Chaser Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCusChaserReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Failed Booking Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isFailBookingReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Failed Booking Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isFailBookingReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Notes Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isNotesReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Notes Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isNotesReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Reservation Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isReservationReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Reservation Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isReservationReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				PrintWriter.append("<tr><td class='fontiiii'>Finance Reports</td><tr>"); 
				
						
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Invoice Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isInvoiceReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Invoice Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isInvoiceReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Passenger Traveled Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isPassengerTraveledReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Passenger Traveled Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isPassengerTraveledReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isPaymentReport_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payment Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isPaymentReport_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Credit Card Details Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCreditCardDetails_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Credit Card Details Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCreditCardDetails_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Prepay Supplier Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isPrepaySupplier_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Prepay Supplier Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isPrepaySupplier_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Supplier Payable Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isSupplierPayable_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Supplier Payable Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isSupplierPayable_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Currency Exchange Rate Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCurrencyExchangeRate_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Currency Exchange Rate Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCurrencyExchangeRate_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				
				
				
				
				
				
				
				
				
				PrintWriter.append("<tr><td class='fontiiii'>Marketing Reports</td><tr>"); 
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Affiliate Commission Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAffiliateCommission_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Affiliate Commission Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAffiliateCommission_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Agent Details Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAgentDetails_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Agent Details Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAgentDetails_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Agent Performance Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAgentPerformance_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Agent Performance Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isAgentPerformance_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Reservation History Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCustomerReservationHistory_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Reservation History Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCustomerReservationHistory_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Usage Statistic Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isUsageStatistic_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Usage Statistic Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isUsageStatistic_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>User Access Audit Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isUserAccessAudit_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>User Access Audit Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isUserAccessAudit_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				/////////////////////////////////////////////////////////////////////////////
				
				PrintWriter.append("<tr><td class='fontiiii'>Contracting Reports</td><tr>"); 
				
				PrintWriter.append("<tr><td class='fontiiiizzz'>Air Module</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Flight Commission Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isFlightCommission_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Flight Commission Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isFlightCommission_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				PrintWriter.append("<tr><td class='fontiiiizzz'>Hotel Module</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Arrival Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelArrival_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Arrival Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelArrival_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - BlackOut Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelBlackOut_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - BlackOut Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelBlackOut_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Commission Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelCommission_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Commission Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelCommission_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Contract Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelContract_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Contract Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelContract_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Customer Fax Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelCustomerFax_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Customer Fax Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelCustomerFax_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Discount Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelDiscount_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Discount Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelDiscount_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Details Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelDetails_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Details Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelDetails_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Inventory Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelInventory_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Inventory Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelInventory_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Profit Markup Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelProfitMarkup_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Profit Markup Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelProfitMarkup_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Rates Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelRates_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Rates Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelRates_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Supplementary Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelSupplementary_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - Supplementary Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isHotelSupplementary_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				PrintWriter.append("<tr><td class='fontiiiizzz'>Activity Module</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Arrival Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityArrival_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Arrival Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityArrival_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - BlackOut Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityBlackOut_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - BlackOut Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityBlackOut_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Commission Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityCommission_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Commission Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityCommission_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Inventory Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityInventory_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Inventory Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityInventory_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Profit MarkUp Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityPM_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Profit MarkUp Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityPM_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Rates Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityRates_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity - Rates Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isActivityRates_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				//
				
				PrintWriter.append("<tr><td class='fontiiiizzz'>Car Module</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Car - Car Commission Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCarCommission_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Car - Car Commission Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCarCommission_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Car - Car Rental Profif Markup Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCarRentalPM_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Car - Car Rental Profif Markup Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isCarRentalPM_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				
				
				PrintWriter.append("<tr><td class='fontiiiizzz'>Supplier Details</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Supplier - Supplier Details Report - Search page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isSupplierDetails_search() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Supplier - Supplier Details Report - Results page</td>");
				PrintWriter.append("<td>Should be Available</td>");
				
				if (reportsDetails.isSupplierDetails_results() == true) {
				
					PrintWriter.append("<td>Available</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				}else{
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			}else{
							
				PrintWriter.append("<td>Login Fail</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");									
			}
			
		
		}else{
			
			PrintWriter.append("<td>System is not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");									
		}
		
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("Report/InternalReport.html")));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();
				
	}
		
}
