package report.reader;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import report.controller.PG_Properties;
import report.model.ReportDetails;

public class CCReportsFlow {
		
	private WebDriver driver         = null;
	private ReportDetails reportsDetails;
	private String currentDateforImages;
	private boolean isPageLoad = false;
	private boolean isLogin = false;
	
	
	public CCReportsFlow(WebDriver driverD){
		this.driver=driverD;
	}
	
	public void getOperationsReports() throws IOException, InterruptedException{
		
		reportsDetails = new ReportDetails();
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
				
		if (isCCPageLoaded(driver) == true) {
			
			File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);						
	        FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/LoginPage.png"));  
	        
	        reportsDetails.setPageLoaded(true);
			
			if (login(driver) == true) {
				
				File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/MainOperationPage.png"));
		        reportsDetails.setLoginSuccess(true);
				
		       
		        //BookingListReport
		        
		        driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=39&reportName=Booking List Report");
				Thread.sleep(1000);
				File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/BookingListReport_Search.png"));
					reportsDetails.setBookingListReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile4 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.className("reportDataRows0"));
						
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/BookingListReport_Results.png"));
						reportsDetails.setBookingListReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/BookingListReport_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/BookingListReport_Search.png"));
					 
				}
				
				
				//Booking Confirmation Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=33&reportName=Booking Confirmation Report");
				Thread.sleep(1000);
				File scrFile5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/BookingConfirmationReport_Search.png"));
					reportsDetails.setBookingConfirmReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/BookingConfirmationReport_Results.png"));
						reportsDetails.setBookingConfirmReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/BookingConfirmationReport_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/BookingConfirmationReport_Search.png"));
					
				}
				
				
				//Quotation Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=27&reportName=Quotation Report");
				Thread.sleep(1000);
				File scrFile7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/QuotationReport_Search.png"));
					reportsDetails.setQuotationReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile8 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/QuotationReport_Results.png"));
						reportsDetails.setQuotationReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/QuotationReport_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/QuotationReport_Search.png"));
					
				}
				
				//Airline Passenger Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/air/AirlinePassengerReport.do?module=operations");
				Thread.sleep(1000);
				File scrFile9 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();

					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/AirPassenger_Search.png"));
					reportsDetails.setAirPass_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile10 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/AirPassenger_Results.png"));
						reportsDetails.setAirPass_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/AirPassenger_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/AirPassenger_Search.png"));
					
				}
		       
				//Auto Cancellation Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=25&reportName=Auto Cancellation Report");
				Thread.sleep(1000);
				File scrFile11 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/AutoCancellation_Search.png"));
					reportsDetails.setAutoCan_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile12 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/AutoCancellation_Results.png"));
						reportsDetails.setAutoCan_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/AutoCancellation_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/AutoCancellation_Search.png"));
					
				}
				    
				//Booking Modification Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=40&reportName=Booking Modification Report");
				Thread.sleep(1000);
				File scrFile13 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile13, new File(""+currentDateforImages+"/BookingModification_Search.png"));
					reportsDetails.setBookingMod_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile14 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile14, new File(""+currentDateforImages+"/BookingModification_Results.png"));
						reportsDetails.setBookingMod_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile14, new File(""+currentDateforImages+"/BookingModification_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile13, new File(""+currentDateforImages+"/BookingModification_Search.png"));
					
				}
				
				//Cancellation Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=1&reportName=Cancellation Report");
				Thread.sleep(1000);
				File scrFile15 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile15, new File(""+currentDateforImages+"/Cancellation_Search.png"));
					reportsDetails.setCancelReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile16 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile16, new File(""+currentDateforImages+"/Cancellation_Results.png"));
						reportsDetails.setCancelReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile16, new File(""+currentDateforImages+"/Cancellation_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile15, new File(""+currentDateforImages+"/Cancellation_Search.png"));
					
				}
				
				//Customer Chaser Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=43&reportName=Customer Chaser Report");
				Thread.sleep(1000);
				File scrFile17 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile17, new File(""+currentDateforImages+"/CustomerChaser_Search.png"));
					reportsDetails.setCusChaserReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile18 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("reportData1"));
						
						FileUtils.copyFile(scrFile18, new File(""+currentDateforImages+"/CustomerChaser_Results.png"));
						reportsDetails.setCusChaserReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile18, new File(""+currentDateforImages+"/CustomerChaser_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile17, new File(""+currentDateforImages+"/CustomerChaser_Search.png"));
					
				}
				
				//Failed Booking Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=45&reportName=Failed Booking Report");
				Thread.sleep(1000);
				File scrFile19 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile19, new File(""+currentDateforImages+"/FailedBooking_Search.png"));
					reportsDetails.setFailBookingReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile20 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile20, new File(""+currentDateforImages+"/FailedBooking_Results.png"));
						reportsDetails.setFailBookingReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile20, new File(""+currentDateforImages+"/FailedBooking_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile19, new File(""+currentDateforImages+"/FailedBooking_Search.png"));
					
				}
				
				//Notes Report 	
				 
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/HotelNoteReport.do?module=operations");
				Thread.sleep(1000);
				File scrFile21 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile21, new File(""+currentDateforImages+"/NotesReport_Search.png"));
					reportsDetails.setNotesReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile22 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/NotesReport_Results.png"));
						reportsDetails.setNotesReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/NotesReport_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile21, new File(""+currentDateforImages+"/NotesReport_Search.png"));
					
				}
				
				//Reservation Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=31&reportName=Reservation Report");
				Thread.sleep(1000);
				File scrFile23 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile23, new File(""+currentDateforImages+"/ReservationReport_Search.png"));
					reportsDetails.setReservationReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile24 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_2"));
						
						FileUtils.copyFile(scrFile24, new File(""+currentDateforImages+"/ReservationReport_Results.png"));
						reportsDetails.setReservationReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile24, new File(""+currentDateforImages+"/ReservationReport_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile23, new File(""+currentDateforImages+"/ReservationReport_Search.png"));
					
				}
				
				//Rooming List Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=22&reportName=Rooming List Report");
				Thread.sleep(1000);
				File scrFile25 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastweek");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile25, new File(""+currentDateforImages+"/RoomingListReport_Search.png"));
					reportsDetails.setRoomingListReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile26 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile26, new File(""+currentDateforImages+"/RoomingListReport_Results.png"));
						reportsDetails.setRoomingListReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile26, new File(""+currentDateforImages+"/RoomingListReport_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile25, new File(""+currentDateforImages+"/RoomingListReport_Search.png"));
					
				}
				
				
	
			}else{	
				
				File scrFileNotLogi = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);	
		        FileUtils.copyFile(scrFileNotLogi, new File(""+currentDateforImages+"/MainOperationPage.png"));
				
			}
			
			
		}else{	
			
			File scrFileNotLoginLoad = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);	
	        FileUtils.copyFile(scrFileNotLoginLoad, new File(""+currentDateforImages+"/LoginPage.png"));
			
		}
		
	}
	
	
	public void getFinanceReports() throws InterruptedException, IOException{
		
		if (isPageLoad == true) {
			
			if (isLogin == true) {
				
			
				//Invoice Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=6&reportName=Invoice Report");
				Thread.sleep(1000);
				File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/InvoiceReport_Search.png"));
					reportsDetails.setInvoiceReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/InvoiceReport_Results.png"));
						reportsDetails.setInvoiceReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/InvoiceReport_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/InvoiceReport_Search.png"));
					 
				}
				
				
				//Passenger Traveled Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/management/MgtPassengerTravelledReport.do?module=finance");
				Thread.sleep(1000);
				File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/PassengerTraveled_Search.png"));
					reportsDetails.setPassengerTraveledReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile4 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/PassengerTraveled_Results.png"));
						reportsDetails.setPassengerTraveledReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/PassengerTraveled_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/PassengerTraveled_Search.png"));
					 
				}
				
				//Payment Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=8&reportName=Payment Report");
				Thread.sleep(1000);
				File scrFile5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='datefilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/Payment_Search.png"));
					reportsDetails.setPaymentReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[3]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[7]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.className("reportDataRows0"));
						
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/Payment_Results.png"));
						reportsDetails.setPaymentReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/Payment_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/Payment_Search.png"));
					 
				}
				
				//Credit Card Details Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=7&reportName=Credit Card Details Report");
				Thread.sleep(1000);
				File scrFile7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='dateFilterRow']/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/CreditCardDetails_Search.png"));
					reportsDetails.setCreditCardDetails_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile8 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/CreditCardDetails_Results.png"));
						reportsDetails.setCreditCardDetails_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/CreditCardDetails_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/CreditCardDetails_Search.png"));
					 
				}
				
				//Prepay Supplier Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/accounting/PrepaySupplierReport.do?module=finance");
				Thread.sleep(1000);
				File scrFile9 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/PrepaySupplier_Search.png"));
					reportsDetails.setPrepaySupplier_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile10 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/PrepaySupplier_Results.png"));
						reportsDetails.setPrepaySupplier_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/PrepaySupplier_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/PrepaySupplier_Search.png"));
					 
				}
				
				
				//Supplier Payable Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=35&reportName=Supplier Payable Report");
				Thread.sleep(1000);
				File scrFile13 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
					
					driver.findElement(By.id("supplierName_suppayable")).sendKeys(PG_Properties.getProperty("SPR_Supplier"));
					driver.findElement(By.xpath(".//*[@id='supplierName_suppayable_lkup']")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					driver.findElement(By.id("currencyCode_suppayable")).sendKeys(PG_Properties.getProperty("SPR_Currency"));
					driver.findElement(By.xpath(".//*[@id='currencyCode_suppayable_lkup']")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
														
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile13, new File(""+currentDateforImages+"/SupplierPayable_Search.png"));
					reportsDetails.setSupplierPayable_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile14 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile14, new File(""+currentDateforImages+"/SupplierPayable_Results.png"));
						reportsDetails.setSupplierPayable_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile14, new File(""+currentDateforImages+"/SupplierPayable_Results.png"));
						
					}
										
				} catch (Exception e) {
					System.out.println(e.getMessage());
					FileUtils.copyFile(scrFile13, new File(""+currentDateforImages+"/SupplierPayable_Search.png"));
					 
				}
				
				
				//Prepay Supplier Report
				
				
				//Credit Limit Report
				
				
				//Currency Exchange Rate Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=42&reportName=Currency Exchange Rate Report");
				Thread.sleep(1000);
				File scrFile11 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/CurrencyExchangeRate_Search.png"));
					reportsDetails.setCurrencyExchangeRate_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile12 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/CurrencyExchangeRate_Results.png"));
						reportsDetails.setCurrencyExchangeRate_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/CurrencyExchangeRate_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/CurrencyExchangeRate_Search.png"));
					 
				}
				
				
				
		
			}
		}
			
	}
	
	
	public void getMarketingReports() throws InterruptedException, IOException{
		
		if (isPageLoad == true) {
			
			if (isLogin == true) {
				
				
				//Affiliate Commission Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=51&reportName=Affiliate Commission Report");
				Thread.sleep(1000);
				File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/AffiliateCommission_Search.png"));
					reportsDetails.setBookingConfirmReport_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/AffiliateCommission_Results.png"));
						reportsDetails.setBookingConfirmReport_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/AffiliateCommission_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/AffiliateCommission_Search.png"));
					
				}
				
				
				
				//Agent Details Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=24&reportName=Agent Details Report");
				Thread.sleep(1000);
				File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
									
					FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/AgentDetails_Search.png"));
					reportsDetails.setAgentDetails_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile4 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/AgentDetails_Results.png"));
						reportsDetails.setAgentDetails_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/AgentDetails_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/AgentDetails_Search.png"));
					
				}
				
				//Agent Performance Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=4&reportName=Agent Performance Report");
				Thread.sleep(1000);
				File scrFile5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
									
					FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/AgentPerformance_Search.png"));
					reportsDetails.setAgentPerformance_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/AgentPerformance_Results.png"));
						reportsDetails.setAgentPerformance_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/AgentPerformance_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/AgentPerformance_Search.png"));
					
				}
				
				
				//Customer Reservation History Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=44&reportName=Customer Reservation History Report");
				Thread.sleep(1000);
				File scrFile7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportfrom_Day_ID']"))).selectByVisibleText("01");
					new Select(driver.findElement(By.xpath(".//*[@id='reportfrom_Month_ID']"))).selectByValue("2");
					new Select(driver.findElement(By.xpath(".//*[@id='reportto_Day_ID']"))).selectByVisibleText("30");
					new Select(driver.findElement(By.xpath(".//*[@id='reportto_Month_ID']"))).selectByValue("2");
					Thread.sleep(1000);
									
					FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/CustomerReservationHistory_Search.png"));
					reportsDetails.setCustomerReservationHistory_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile8 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/CustomerReservationHistory_Results.png"));
						reportsDetails.setCustomerReservationHistory_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/CustomerReservationHistory_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/CustomerReservationHistory_Search.png"));
					
				}
				
				//Usage Statistic Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=29&reportName=Usage Statistic Report");
				Thread.sleep(1000);
				File scrFile9 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
					
					FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/UsageStatistic_Search.png"));
					reportsDetails.setUsageStatistic_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile10 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/UsageStatistic_Results.png"));
						reportsDetails.setUsageStatistic_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/UsageStatistic_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/UsageStatistic_Search.png"));
					
				}
				
				
				//User Access Audit Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=30&reportName=User Access Audit Report");
				Thread.sleep(1000);
				File scrFile11 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");	
					
					driver.findElement(By.id("auditscreenname")).sendKeys("Hotel Setup - Hotel Info");
					driver.findElement(By.xpath(".//*[@id='auditscreenname_lkup']")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();					
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
										
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/UserAccessAudit_Search.png"));
					reportsDetails.setUserAccessAudit_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile12 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/UserAccessAudit_Results.png"));
						reportsDetails.setUserAccessAudit_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/UserAccessAudit_Results.png"));
						
					}
									
				} catch (Exception e) {
					FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/UserAccessAudit_Search.png"));
					
				}
				
				
			}
		}
				
		
	}
	
	
	public ReportDetails getContractingReports() throws InterruptedException, IOException{
		
		if (isPageLoad == true) {
			
			if (isLogin == true) {
				
				
				//Flight Commission Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/air/FlightCommissionaReport.do?module=contract");
				Thread.sleep(1000);
				File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/Air_FlightCommission_Search.png"));
					reportsDetails.setFlightCommission_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_2"));
						
						FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/Air_FlightCommission_Results.png"));
						reportsDetails.setFlightCommission_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile2, new File(""+currentDateforImages+"/Air_FlightCommission_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile1, new File(""+currentDateforImages+"/Air_FlightCommission_Search.png"));
					 
				}
				
				//Hotel - Arrival Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=14&reportName=Arrival Report");
				Thread.sleep(1000);
				File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/Hotel_Arrival_Search.png"));
					reportsDetails.setHotelArrival_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile4 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/Hotel_Arrival_Results.png"));
						reportsDetails.setHotelArrival_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile4, new File(""+currentDateforImages+"/Hotel_Arrival_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile3, new File(""+currentDateforImages+"/Hotel_Arrival_Search.png"));
					 
				}
				
				
				//Hotel - BlackOut Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=15&reportName=Blackout Report");
				Thread.sleep(1000);
				File scrFile5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/Hotel_BlackOut_Search.png"));
					reportsDetails.setHotelBlackOut_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/Hotel_BlackOut_Results.png"));
						reportsDetails.setHotelBlackOut_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile6, new File(""+currentDateforImages+"/Hotel_BlackOut_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile5, new File(""+currentDateforImages+"/Hotel_BlackOut_Search.png"));
					 
				}
				
				//Hotel - Commission Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=16&reportName=Commission Report");
				Thread.sleep(1000);
				File scrFile7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/Hotel_Commission_Search.png"));
					reportsDetails.setHotelCommission_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile8 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/Hotel_Commission_Results.png"));
						reportsDetails.setHotelCommission_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile8, new File(""+currentDateforImages+"/Hotel_Commission_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile7, new File(""+currentDateforImages+"/Hotel_Commission_Search.png"));
					 
				}
				
				
				//Hotel - Contract Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=41&reportName=Contract Report");
				Thread.sleep(1000);
				File scrFile9 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/Hotel_Contract_Search.png"));
					reportsDetails.setHotelContract_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile10 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/Hotel_Contract_Results.png"));
						reportsDetails.setHotelContract_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile10, new File(""+currentDateforImages+"/Hotel_Contract_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile9, new File(""+currentDateforImages+"/Hotel_Contract_Search.png"));
					 
				}
				
				
				//Hotel - Customer Fax Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/hotels/OpsCustomerFaxReport.do?module=contract");
				Thread.sleep(1000);
				File scrFile11 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/Hotel_CustomerFax_Search.png"));
					reportsDetails.setHotelCustomerFax_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile12 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/Hotel_CustomerFax_Results.png"));
						reportsDetails.setHotelCustomerFax_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile12, new File(""+currentDateforImages+"/Hotel_CustomerFax_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile11, new File(""+currentDateforImages+"/Hotel_CustomerFax_Search.png"));
					 
				}
				
				//Hotel - Discount Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=17&reportName=Discount Report");
				Thread.sleep(1000);
				File scrFile13 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile13, new File(""+currentDateforImages+"/Hotel_Discount_Search.png"));
					reportsDetails.setHotelDiscount_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile14 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile14, new File(""+currentDateforImages+"/Hotel_Discount_Results.png"));
						reportsDetails.setHotelDiscount_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile14, new File(""+currentDateforImages+"/Hotel_Discount_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile13, new File(""+currentDateforImages+"/Hotel_Discount_Search.png"));
					 
				}
				
				//Hotel - Details Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=18&reportName=Hotel Details Report");
				Thread.sleep(1000);
				File scrFile15 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					FileUtils.copyFile(scrFile15, new File(""+currentDateforImages+"/Hotel_Details_Search.png"));
					reportsDetails.setHotelDetails_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile16 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile16, new File(""+currentDateforImages+"/Hotel_Details_Results.png"));
						reportsDetails.setHotelDetails_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile16, new File(""+currentDateforImages+"/Hotel_Details_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile15, new File(""+currentDateforImages+"/Hotel_Details_Search.png"));
					 
				}
				
				
				//Hotel - Inventory Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=19&reportName=Inventory Report");
				Thread.sleep(1000);
				File scrFile17 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile17, new File(""+currentDateforImages+"/Hotel_Inventory_Search.png"));
					reportsDetails.setHotelInventory_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile18 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile18, new File(""+currentDateforImages+"/Hotel_Inventory_Results.png"));
						reportsDetails.setHotelInventory_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile18, new File(""+currentDateforImages+"/Hotel_Inventory_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile17, new File(""+currentDateforImages+"/Hotel_Inventory_Search.png"));
					 
				}
				
				//Hotel - Profit Markup Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=20&reportName=Profit Markup Report");
				Thread.sleep(1000);
				File scrFile19 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile19, new File(""+currentDateforImages+"/Hotel_ProfitMarkup_Search.png"));
					reportsDetails.setHotelProfitMarkup_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile20 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile20, new File(""+currentDateforImages+"/Hotel_ProfitMarkup_Results.png"));
						reportsDetails.setHotelProfitMarkup_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile20, new File(""+currentDateforImages+"/Hotel_ProfitMarkup_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile19, new File(""+currentDateforImages+"/Hotel_ProfitMarkup_Search.png"));
					 
				}
				
				
				//Hotel - Rates Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=21&reportName=Rates Report");
				Thread.sleep(1000);
				File scrFile21 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile21, new File(""+currentDateforImages+"/Hotel_Rates_Search.png"));
					reportsDetails.setHotelRates_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile22 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/Hotel_Rates_Results.png"));
						reportsDetails.setHotelRates_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/Hotel_Rates_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile21, new File(""+currentDateforImages+"/Hotel_Rates_Search.png"));
					 
				}
				
				//Hotel - Supplementary Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=23&reportName=Supplementary Report");
				Thread.sleep(1000);
				File scrFile23 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					FileUtils.copyFile(scrFile23, new File(""+currentDateforImages+"/Hotel_Supplementary_Search.png"));
					reportsDetails.setHotelRates_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile24 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile24, new File(""+currentDateforImages+"/Hotel_Supplementary_Results.png"));
						reportsDetails.setHotelRates_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile24, new File(""+currentDateforImages+"/Hotel_Supplementary_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile23, new File(""+currentDateforImages+"/Hotel_Supplementary_Search.png"));
					 
				}
				
				//Activity - Arrival Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=5&reportName=Arrival Report");
				Thread.sleep(1000);
				File scrFile25 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile25, new File(""+currentDateforImages+"/Activity_Arrival_Search.png"));
					reportsDetails.setActivityArrival_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile26 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile26, new File(""+currentDateforImages+"/Activity_Arrival_Results.png"));
						reportsDetails.setActivityArrival_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile26, new File(""+currentDateforImages+"/Activity_Arrival_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile25, new File(""+currentDateforImages+"/Activity_Arrival_Search.png"));
					 
				}
				
				
				//Activity - BlackOut Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=9&reportName=Blackout Report");
				Thread.sleep(1000);
				File scrFile27 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile27, new File(""+currentDateforImages+"/Activity_BlackOut_Search.png"));
					reportsDetails.setActivityBlackOut_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile28 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile28, new File(""+currentDateforImages+"/Activity_BlackOut_Results.png"));
						reportsDetails.setActivityBlackOut_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile28, new File(""+currentDateforImages+"/Activity_BlackOut_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile27, new File(""+currentDateforImages+"/Activity_BlackOut_Search.png"));
					 
				}
				
				
				//Activity - Commission Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=10&reportName=Commission Report");
				Thread.sleep(1000);
				File scrFile29 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile29, new File(""+currentDateforImages+"/Activity_Commission_Search.png"));
					reportsDetails.setActivityCommission_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile30 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile30, new File(""+currentDateforImages+"/Activity_Commission_Results.png"));
						reportsDetails.setActivityCommission_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile30, new File(""+currentDateforImages+"/Activity_Commission_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile29, new File(""+currentDateforImages+"/Activity_Commission_Search.png"));
					 
				}
				
				
				//Activity - Inventory Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=11&reportName=Inventory Report");
				Thread.sleep(1000);
				File scrFile31 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile31, new File(""+currentDateforImages+"/Activity_Inventory_Search.png"));
					reportsDetails.setActivityInventory_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile32 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile32, new File(""+currentDateforImages+"/Activity_Inventory_Results.png"));
						reportsDetails.setActivityInventory_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile32, new File(""+currentDateforImages+"/Activity_Inventory_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile31, new File(""+currentDateforImages+"/Activity_Inventory_Search.png"));
					 
				}
				
				
				//Activity - Profit MarkUp Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=12&reportName=Profit Markup Report");
				Thread.sleep(1000);
				File scrFile33 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile33, new File(""+currentDateforImages+"/Activity_ProfitMarkUp_Search.png"));
					reportsDetails.setActivityPM_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile34 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile34, new File(""+currentDateforImages+"/Activity_ProfitMarkUp_Results.png"));
						reportsDetails.setActivityPM_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile34, new File(""+currentDateforImages+"/Activity_ProfitMarkUp_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile33, new File(""+currentDateforImages+"/Activity_ProfitMarkUp_Search.png"));
					 
				}
				
				
				//Activity - Rates Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=13&reportName=Rates Report");
				Thread.sleep(1000);
				File scrFile35 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile35, new File(""+currentDateforImages+"/Activity_Rates_Search.png"));
					reportsDetails.setActivityRates_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile36 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile36, new File(""+currentDateforImages+"/Activity_Rates_Results.png"));
						reportsDetails.setActivityRates_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile36, new File(""+currentDateforImages+"/Activity_Rates_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile35, new File(""+currentDateforImages+"/Activity_ProfitMarkUp_Search.png"));
					 
				}
				
				////
				
				//Supplier - Supplier Details Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/operational/mainReport.do?reportId=28&reportName=Supplier Details Report");
				Thread.sleep(1000);
				File scrFile37 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					driver.switchTo().frame("reportIframe");
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile37, new File(""+currentDateforImages+"/SupplierDetails_Search.png"));
					reportsDetails.setSupplierDetails_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile38 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.switchTo().frame("reportIframe");
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile38, new File(""+currentDateforImages+"/SupplierDetails_Results.png"));
						reportsDetails.setSupplierDetails_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile38, new File(""+currentDateforImages+"/SupplierDetails_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile37, new File(""+currentDateforImages+"/SupplierDetails_Search.png"));
					 
				}
				
				////
				
				//Car - Car Commission Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/car/CarCommissionReport.do?module=contract");
				Thread.sleep(1000);
				File scrFile39 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					
					new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("lastmonth");
					Thread.sleep(1000);
					
					FileUtils.copyFile(scrFile39, new File(""+currentDateforImages+"/CarCommissionReport_Search.png"));
					reportsDetails.setCarCommission_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile40 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_2"));
						
						FileUtils.copyFile(scrFile40, new File(""+currentDateforImages+"/CarCommissionReport_Results.png"));
						reportsDetails.setCarCommission_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile40, new File(""+currentDateforImages+"/CarCommissionReport_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile39, new File(""+currentDateforImages+"/CarCommissionReport_Search.png"));
					 
				}
				
				
			
				//Car - Car Rental Profif Markup Report
				
				driver.get(PG_Properties.getProperty("Baseurl") + "/reports/car/CarRentalProfitMarkupReport.do?module=contract");
				Thread.sleep(1000);
				File scrFile41 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				
				try {
					
					driver.switchTo().defaultContent();
					
					FileUtils.copyFile(scrFile41, new File(""+currentDateforImages+"/CarRentalProfitMarkupReport_Search.png"));
					reportsDetails.setCarRentalPM_search(true);
					
					driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
					Thread.sleep(3000);
					
					File scrFile42 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					
					try {
						
						driver.switchTo().defaultContent();
						driver.findElement(By.id("RowNo_1_1"));
						
						FileUtils.copyFile(scrFile42, new File(""+currentDateforImages+"/CarRentalProfitMarkupReport_Results.png"));
						reportsDetails.setCarRentalPM_results(true);
											
					} catch (Exception e) {
						FileUtils.copyFile(scrFile42, new File(""+currentDateforImages+"/CarRentalProfitMarkupReport_Results.png"));
						
					}
										
				} catch (Exception e) {
					 FileUtils.copyFile(scrFile41, new File(""+currentDateforImages+"/CarRentalProfitMarkupReport_Search.png"));
					 
				}
				
				
				
				
				
				
				
				
				
			}
		}
				
		return reportsDetails;
	}
	
	

	
	public boolean isCCPageLoaded(WebDriver driver_1) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		Thread.sleep(1000);
		
		try {			
			WebElement idElement = driver.findElement(By.id("user_id"));
			idElement.isDisplayed();
			isPageLoad = true;
			return true;
			
		} catch (Exception e) {
			return false;
		}		
	}
	
	
	public boolean login(WebDriver driver_1) throws InterruptedException{
	
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("UserName"));
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
		driver.findElement(By.id("loginbutton")).click();
		Thread.sleep(1500);
		
		try {			
			driver.findElement(By.id("mainmenurezglogo"));
			isLogin = true;
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}
	
}
