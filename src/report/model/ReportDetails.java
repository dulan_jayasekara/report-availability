package report.model;

public class ReportDetails {
	
	private boolean isPageLoaded = false;
	private boolean isLoginSuccess = false;
	
	private boolean isBookingListReport_search = false;
	private boolean isBookingListReport_results = false;
	
	private boolean isBookingConfirmReport_search = false;
	private boolean isBookingConfirmReport_results = false;
	
	private boolean isQuotationReport_search = false;
	private boolean isQuotationReport_results = false;
	
	private boolean isAirPass_search = false;
	private boolean isAirPass_results = false;
	
	private boolean isAutoCan_search = false;
	private boolean isAutoCan_results = false;
	
	private boolean isBookingMod_search = false;
	private boolean isBookingMod_results = false;
	
	private boolean isCancelReport_search = false;
	private boolean isCancelReport_results = false;
	
	private boolean isCusChaserReport_search = false;
	private boolean isCusChaserReport_results = false;
	
	private boolean isFailBookingReport_search = false;
	private boolean isFailBookingReport_results = false;
	
	private boolean isNotesReport_search = false;
	private boolean isNotesReport_results = false;
	
	private boolean isReservationReport_search = false;
	private boolean isReservationReport_results = false;
	
	private boolean isRoomingListReport_search = false;
	private boolean isRoomingListReport_results = false;
	
	private boolean isInvoiceReport_search = false;
	private boolean isInvoiceReport_results = false;
	
	private boolean isPassengerTraveledReport_search = false;
	private boolean isPassengerTraveledReport_results = false;
	
	private boolean isPaymentReport_search = false;
	private boolean isPaymentReport_results = false;
	
	private boolean isCreditCardDetails_search = false;
	private boolean isCreditCardDetails_results = false;
	
	private boolean isPrepaySupplier_search = false;
	private boolean isPrepaySupplier_results = false;
	
	private boolean isSupplierPayable_search = false;
	private boolean isSupplierPayable_results = false;
	
	private boolean isCurrencyExchangeRate_search = false;
	private boolean isCurrencyExchangeRate_results = false;
	
	//
	
	
	private boolean isAffiliateCommission_search = false;
	private boolean isAffiliateCommission_results = false;
	
	private boolean isAgentDetails_search = false;
	private boolean isAgentDetails_results = false;
	
	private boolean isAgentPerformance_search = false;
	private boolean isAgentPerformance_results = false;
	
	private boolean isCustomerReservationHistory_search = false;
	private boolean isCustomerReservationHistory_results = false;
	
	private boolean isUsageStatistic_search = false;
	private boolean isUsageStatistic_results = false;
	
	private boolean isUserAccessAudit_search = false;
	private boolean isUserAccessAudit_results = false;
	
	////
	
	private boolean isFlightCommission_search = false;
	private boolean isFlightCommission_results = false;
	
	private boolean isHotelArrival_search = false;
	private boolean isHotelArrival_results = false;
	
	private boolean isHotelBlackOut_search = false;
	private boolean isHotelBlackOut_results = false;
	
	private boolean isHotelCommission_search = false;
	private boolean isHotelCommission_results = false;
	
	private boolean isHotelContract_search = false;
	private boolean isHotelContract_results = false;
	
	private boolean isHotelCustomerFax_search = false;
	private boolean isHotelCustomerFax_results = false;
	
	private boolean isHotelDiscount_search = false;
	private boolean isHotelDiscount_results = false;
	
	private boolean isHotelDetails_search = false;
	private boolean isHotelDetails_results = false;
	
	private boolean isHotelInventory_search = false;
	private boolean isHotelInventory_results = false; 
	
	private boolean isHotelProfitMarkup_search = false;
	private boolean isHotelProfitMarkup_results = false; 
	
	private boolean isHotelRates_search = false;
	private boolean isHotelRates_results = false; 
	
	private boolean isHotelSupplementary_search = false;
	private boolean isHotelSupplementary_results = false; 
	
	///////
	
	private boolean isActivityArrival_search = false;
	private boolean isActivityArrival_results = false; 
	
	private boolean isActivityBlackOut_search = false;
	private boolean isActivityBlackOut_results = false; 
	
	private boolean isActivityCommission_search = false;
	private boolean isActivityCommission_results = false; 
	
	private boolean isActivityInventory_search = false;
	private boolean isActivityInventory_results = false; 
	
	private boolean isActivityPM_search = false;
	private boolean isActivityPM_results = false; 
	
	private boolean isActivityRates_search = false;
	private boolean isActivityRates_results = false; 
	
	private boolean isSupplierDetails_search = false;
	private boolean isSupplierDetails_results = false; 
	
	//
	
	private boolean isCarCommission_search = false;
	private boolean isCarCommission_results = false; 
	
	private boolean isCarRentalPM_search = false;
	private boolean isCarRentalPM_results = false; 
	
	
	
	
	
	
	
	
	
	
	public boolean isCarRentalPM_search() {
		return isCarRentalPM_search;
	}
	public void setCarRentalPM_search(boolean isCarRentalPM_search) {
		this.isCarRentalPM_search = isCarRentalPM_search;
	}
	public boolean isCarRentalPM_results() {
		return isCarRentalPM_results;
	}
	public void setCarRentalPM_results(boolean isCarRentalPM_results) {
		this.isCarRentalPM_results = isCarRentalPM_results;
	}
	public boolean isCarCommission_search() {
		return isCarCommission_search;
	}
	public void setCarCommission_search(boolean isCarCommission_search) {
		this.isCarCommission_search = isCarCommission_search;
	}
	public boolean isCarCommission_results() {
		return isCarCommission_results;
	}
	public void setCarCommission_results(boolean isCarCommission_results) {
		this.isCarCommission_results = isCarCommission_results;
	}
	public boolean isSupplierDetails_search() {
		return isSupplierDetails_search;
	}
	public void setSupplierDetails_search(boolean isSupplierDetails_search) {
		this.isSupplierDetails_search = isSupplierDetails_search;
	}
	public boolean isSupplierDetails_results() {
		return isSupplierDetails_results;
	}
	public void setSupplierDetails_results(boolean isSupplierDetails_results) {
		this.isSupplierDetails_results = isSupplierDetails_results;
	}
	public boolean isActivityArrival_search() {
		return isActivityArrival_search;
	}
	public void setActivityArrival_search(boolean isActivityArrival_search) {
		this.isActivityArrival_search = isActivityArrival_search;
	}
	public boolean isActivityArrival_results() {
		return isActivityArrival_results;
	}
	public void setActivityArrival_results(boolean isActivityArrival_results) {
		this.isActivityArrival_results = isActivityArrival_results;
	}
	public boolean isActivityBlackOut_search() {
		return isActivityBlackOut_search;
	}
	public void setActivityBlackOut_search(boolean isActivityBlackOut_search) {
		this.isActivityBlackOut_search = isActivityBlackOut_search;
	}
	public boolean isActivityBlackOut_results() {
		return isActivityBlackOut_results;
	}
	public void setActivityBlackOut_results(boolean isActivityBlackOut_results) {
		this.isActivityBlackOut_results = isActivityBlackOut_results;
	}
	public boolean isActivityCommission_search() {
		return isActivityCommission_search;
	}
	public void setActivityCommission_search(boolean isActivityCommission_search) {
		this.isActivityCommission_search = isActivityCommission_search;
	}
	public boolean isActivityCommission_results() {
		return isActivityCommission_results;
	}
	public void setActivityCommission_results(boolean isActivityCommission_results) {
		this.isActivityCommission_results = isActivityCommission_results;
	}
	public boolean isActivityInventory_search() {
		return isActivityInventory_search;
	}
	public void setActivityInventory_search(boolean isActivityInventory_search) {
		this.isActivityInventory_search = isActivityInventory_search;
	}
	public boolean isActivityInventory_results() {
		return isActivityInventory_results;
	}
	public void setActivityInventory_results(boolean isActivityInventory_results) {
		this.isActivityInventory_results = isActivityInventory_results;
	}
	public boolean isActivityPM_search() {
		return isActivityPM_search;
	}
	public void setActivityPM_search(boolean isActivityPM_search) {
		this.isActivityPM_search = isActivityPM_search;
	}
	public boolean isActivityPM_results() {
		return isActivityPM_results;
	}
	public void setActivityPM_results(boolean isActivityPM_results) {
		this.isActivityPM_results = isActivityPM_results;
	}
	public boolean isActivityRates_search() {
		return isActivityRates_search;
	}
	public void setActivityRates_search(boolean isActivityRates_search) {
		this.isActivityRates_search = isActivityRates_search;
	}
	public boolean isActivityRates_results() {
		return isActivityRates_results;
	}
	public void setActivityRates_results(boolean isActivityRates_results) {
		this.isActivityRates_results = isActivityRates_results;
	}
	public boolean isHotelSupplementary_search() {
		return isHotelSupplementary_search;
	}
	public void setHotelSupplementary_search(boolean isHotelSupplementary_search) {
		this.isHotelSupplementary_search = isHotelSupplementary_search;
	}
	public boolean isHotelSupplementary_results() {
		return isHotelSupplementary_results;
	}
	public void setHotelSupplementary_results(boolean isHotelSupplementary_results) {
		this.isHotelSupplementary_results = isHotelSupplementary_results;
	}
	public boolean isHotelRates_search() {
		return isHotelRates_search;
	}
	public void setHotelRates_search(boolean isHotelRates_search) {
		this.isHotelRates_search = isHotelRates_search;
	}
	public boolean isHotelRates_results() {
		return isHotelRates_results;
	}
	public void setHotelRates_results(boolean isHotelRates_results) {
		this.isHotelRates_results = isHotelRates_results;
	}
	public boolean isHotelProfitMarkup_search() {
		return isHotelProfitMarkup_search;
	}
	public void setHotelProfitMarkup_search(boolean isHotelProfitMarkup_search) {
		this.isHotelProfitMarkup_search = isHotelProfitMarkup_search;
	}
	public boolean isHotelProfitMarkup_results() {
		return isHotelProfitMarkup_results;
	}
	public void setHotelProfitMarkup_results(boolean isHotelProfitMarkup_results) {
		this.isHotelProfitMarkup_results = isHotelProfitMarkup_results;
	}
	public boolean isHotelInventory_search() {
		return isHotelInventory_search;
	}
	public void setHotelInventory_search(boolean isHotelInventory_search) {
		this.isHotelInventory_search = isHotelInventory_search;
	}
	public boolean isHotelInventory_results() {
		return isHotelInventory_results;
	}
	public void setHotelInventory_results(boolean isHotelInventory_results) {
		this.isHotelInventory_results = isHotelInventory_results;
	}
	public boolean isHotelDetails_search() {
		return isHotelDetails_search;
	}
	public void setHotelDetails_search(boolean isHotelDetails_search) {
		this.isHotelDetails_search = isHotelDetails_search;
	}
	public boolean isHotelDetails_results() {
		return isHotelDetails_results;
	}
	public void setHotelDetails_results(boolean isHotelDetails_results) {
		this.isHotelDetails_results = isHotelDetails_results;
	}
	public boolean isHotelDiscount_search() {
		return isHotelDiscount_search;
	}
	public void setHotelDiscount_search(boolean isHotelDiscount_search) {
		this.isHotelDiscount_search = isHotelDiscount_search;
	}
	public boolean isHotelDiscount_results() {
		return isHotelDiscount_results;
	}
	public void setHotelDiscount_results(boolean isHotelDiscount_results) {
		this.isHotelDiscount_results = isHotelDiscount_results;
	}
	public boolean isHotelCustomerFax_search() {
		return isHotelCustomerFax_search;
	}
	public void setHotelCustomerFax_search(boolean isHotelCustomerFax_search) {
		this.isHotelCustomerFax_search = isHotelCustomerFax_search;
	}
	public boolean isHotelCustomerFax_results() {
		return isHotelCustomerFax_results;
	}
	public void setHotelCustomerFax_results(boolean isHotelCustomerFax_results) {
		this.isHotelCustomerFax_results = isHotelCustomerFax_results;
	}
	public boolean isHotelContract_search() {
		return isHotelContract_search;
	}
	public void setHotelContract_search(boolean isHotelContract_search) {
		this.isHotelContract_search = isHotelContract_search;
	}
	public boolean isHotelContract_results() {
		return isHotelContract_results;
	}
	public void setHotelContract_results(boolean isHotelContract_results) {
		this.isHotelContract_results = isHotelContract_results;
	}
	public boolean isHotelCommission_search() {
		return isHotelCommission_search;
	}
	public void setHotelCommission_search(boolean isHotelCommission_search) {
		this.isHotelCommission_search = isHotelCommission_search;
	}
	public boolean isHotelCommission_results() {
		return isHotelCommission_results;
	}
	public void setHotelCommission_results(boolean isHotelCommission_results) {
		this.isHotelCommission_results = isHotelCommission_results;
	}
	public boolean isHotelBlackOut_search() {
		return isHotelBlackOut_search;
	}
	public void setHotelBlackOut_search(boolean isHotelBlackOut_search) {
		this.isHotelBlackOut_search = isHotelBlackOut_search;
	}
	public boolean isHotelBlackOut_results() {
		return isHotelBlackOut_results;
	}
	public void setHotelBlackOut_results(boolean isHotelBlackOut_results) {
		this.isHotelBlackOut_results = isHotelBlackOut_results;
	}
	public boolean isHotelArrival_search() {
		return isHotelArrival_search;
	}
	public void setHotelArrival_search(boolean isHotelArrival_search) {
		this.isHotelArrival_search = isHotelArrival_search;
	}
	public boolean isHotelArrival_results() {
		return isHotelArrival_results;
	}
	public void setHotelArrival_results(boolean isHotelArrival_results) {
		this.isHotelArrival_results = isHotelArrival_results;
	}
	public boolean isFlightCommission_search() {
		return isFlightCommission_search;
	}
	public void setFlightCommission_search(boolean isFlightCommission_search) {
		this.isFlightCommission_search = isFlightCommission_search;
	}
	public boolean isFlightCommission_results() {
		return isFlightCommission_results;
	}
	public void setFlightCommission_results(boolean isFlightCommission_results) {
		this.isFlightCommission_results = isFlightCommission_results;
	}
	public boolean isUserAccessAudit_search() {
		return isUserAccessAudit_search;
	}
	public void setUserAccessAudit_search(boolean isUserAccessAudit_search) {
		this.isUserAccessAudit_search = isUserAccessAudit_search;
	}
	public boolean isUserAccessAudit_results() {
		return isUserAccessAudit_results;
	}
	public void setUserAccessAudit_results(boolean isUserAccessAudit_results) {
		this.isUserAccessAudit_results = isUserAccessAudit_results;
	}
	public boolean isUsageStatistic_search() {
		return isUsageStatistic_search;
	}
	public void setUsageStatistic_search(boolean isUsageStatistic_search) {
		this.isUsageStatistic_search = isUsageStatistic_search;
	}
	public boolean isUsageStatistic_results() {
		return isUsageStatistic_results;
	}
	public void setUsageStatistic_results(boolean isUsageStatistic_results) {
		this.isUsageStatistic_results = isUsageStatistic_results;
	}
	public boolean isCustomerReservationHistory_search() {
		return isCustomerReservationHistory_search;
	}
	public void setCustomerReservationHistory_search(
			boolean isCustomerReservationHistory_search) {
		this.isCustomerReservationHistory_search = isCustomerReservationHistory_search;
	}
	public boolean isCustomerReservationHistory_results() {
		return isCustomerReservationHistory_results;
	}
	public void setCustomerReservationHistory_results(
			boolean isCustomerReservationHistory_results) {
		this.isCustomerReservationHistory_results = isCustomerReservationHistory_results;
	}
	public boolean isAgentPerformance_search() {
		return isAgentPerformance_search;
	}
	public void setAgentPerformance_search(boolean isAgentPerformance_search) {
		this.isAgentPerformance_search = isAgentPerformance_search;
	}
	public boolean isAgentPerformance_results() {
		return isAgentPerformance_results;
	}
	public void setAgentPerformance_results(boolean isAgentPerformance_results) {
		this.isAgentPerformance_results = isAgentPerformance_results;
	}
	public boolean isAgentDetails_search() {
		return isAgentDetails_search;
	}
	public void setAgentDetails_search(boolean isAgentDetails_search) {
		this.isAgentDetails_search = isAgentDetails_search;
	}
	public boolean isAgentDetails_results() {
		return isAgentDetails_results;
	}
	public void setAgentDetails_results(boolean isAgentDetails_results) {
		this.isAgentDetails_results = isAgentDetails_results;
	}
	public boolean isAffiliateCommission_search() {
		return isAffiliateCommission_search;
	}
	public void setAffiliateCommission_search(boolean isAffiliateCommission_search) {
		this.isAffiliateCommission_search = isAffiliateCommission_search;
	}
	public boolean isAffiliateCommission_results() {
		return isAffiliateCommission_results;
	}
	public void setAffiliateCommission_results(boolean isAffiliateCommission_results) {
		this.isAffiliateCommission_results = isAffiliateCommission_results;
	}
	public boolean isCurrencyExchangeRate_search() {
		return isCurrencyExchangeRate_search;
	}
	public void setCurrencyExchangeRate_search(boolean isCurrencyExchangeRate_search) {
		this.isCurrencyExchangeRate_search = isCurrencyExchangeRate_search;
	}
	public boolean isCurrencyExchangeRate_results() {
		return isCurrencyExchangeRate_results;
	}
	public void setCurrencyExchangeRate_results(
			boolean isCurrencyExchangeRate_results) {
		this.isCurrencyExchangeRate_results = isCurrencyExchangeRate_results;
	}
	public boolean isSupplierPayable_search() {
		return isSupplierPayable_search;
	}
	public void setSupplierPayable_search(boolean isSupplierPayable_search) {
		this.isSupplierPayable_search = isSupplierPayable_search;
	}
	public boolean isSupplierPayable_results() {
		return isSupplierPayable_results;
	}
	public void setSupplierPayable_results(boolean isSupplierPayable_results) {
		this.isSupplierPayable_results = isSupplierPayable_results;
	}
	public boolean isPrepaySupplier_search() {
		return isPrepaySupplier_search;
	}
	public void setPrepaySupplier_search(boolean isPrepaySupplier_search) {
		this.isPrepaySupplier_search = isPrepaySupplier_search;
	}
	public boolean isPrepaySupplier_results() {
		return isPrepaySupplier_results;
	}
	public void setPrepaySupplier_results(boolean isPrepaySupplier_results) {
		this.isPrepaySupplier_results = isPrepaySupplier_results;
	}
	public boolean isCreditCardDetails_search() {
		return isCreditCardDetails_search;
	}
	public void setCreditCardDetails_search(boolean isCreditCardDetails_search) {
		this.isCreditCardDetails_search = isCreditCardDetails_search;
	}
	public boolean isCreditCardDetails_results() {
		return isCreditCardDetails_results;
	}
	public void setCreditCardDetails_results(boolean isCreditCardDetails_results) {
		this.isCreditCardDetails_results = isCreditCardDetails_results;
	}
	public boolean isPaymentReport_search() {
		return isPaymentReport_search;
	}
	public void setPaymentReport_search(boolean isPaymentReport_search) {
		this.isPaymentReport_search = isPaymentReport_search;
	}
	public boolean isPaymentReport_results() {
		return isPaymentReport_results;
	}
	public void setPaymentReport_results(boolean isPaymentReport_results) {
		this.isPaymentReport_results = isPaymentReport_results;
	}
	public boolean isPassengerTraveledReport_search() {
		return isPassengerTraveledReport_search;
	}
	public void setPassengerTraveledReport_search(
			boolean isPassengerTraveledReport_search) {
		this.isPassengerTraveledReport_search = isPassengerTraveledReport_search;
	}
	public boolean isPassengerTraveledReport_results() {
		return isPassengerTraveledReport_results;
	}
	public void setPassengerTraveledReport_results(
			boolean isPassengerTraveledReport_results) {
		this.isPassengerTraveledReport_results = isPassengerTraveledReport_results;
	}
	public boolean isInvoiceReport_search() {
		return isInvoiceReport_search;
	}
	public void setInvoiceReport_search(boolean isInvoiceReport_search) {
		this.isInvoiceReport_search = isInvoiceReport_search;
	}
	public boolean isInvoiceReport_results() {
		return isInvoiceReport_results;
	}
	public void setInvoiceReport_results(boolean isInvoiceReport_results) {
		this.isInvoiceReport_results = isInvoiceReport_results;
	}
	public boolean isRoomingListReport_search() {
		return isRoomingListReport_search;
	}
	public void setRoomingListReport_search(boolean isRoomingListReport_search) {
		this.isRoomingListReport_search = isRoomingListReport_search;
	}
	public boolean isRoomingListReport_results() {
		return isRoomingListReport_results;
	}
	public void setRoomingListReport_results(boolean isRoomingListReport_results) {
		this.isRoomingListReport_results = isRoomingListReport_results;
	}
	public boolean isReservationReport_search() {
		return isReservationReport_search;
	}
	public void setReservationReport_search(boolean isReservationReport_search) {
		this.isReservationReport_search = isReservationReport_search;
	}
	public boolean isReservationReport_results() {
		return isReservationReport_results;
	}
	public void setReservationReport_results(boolean isReservationReport_results) {
		this.isReservationReport_results = isReservationReport_results;
	}
	public boolean isNotesReport_search() {
		return isNotesReport_search;
	}
	public void setNotesReport_search(boolean isNotesReport_search) {
		this.isNotesReport_search = isNotesReport_search;
	}
	public boolean isNotesReport_results() {
		return isNotesReport_results;
	}
	public void setNotesReport_results(boolean isNotesReport_results) {
		this.isNotesReport_results = isNotesReport_results;
	}
	public boolean isFailBookingReport_search() {
		return isFailBookingReport_search;
	}
	public void setFailBookingReport_search(boolean isFailBookingReport_search) {
		this.isFailBookingReport_search = isFailBookingReport_search;
	}
	public boolean isFailBookingReport_results() {
		return isFailBookingReport_results;
	}
	public void setFailBookingReport_results(boolean isFailBookingReport_results) {
		this.isFailBookingReport_results = isFailBookingReport_results;
	}
	public boolean isCusChaserReport_search() {
		return isCusChaserReport_search;
	}
	public void setCusChaserReport_search(boolean isCusChaserReport_search) {
		this.isCusChaserReport_search = isCusChaserReport_search;
	}
	public boolean isCusChaserReport_results() {
		return isCusChaserReport_results;
	}
	public void setCusChaserReport_results(boolean isCusChaserReport_results) {
		this.isCusChaserReport_results = isCusChaserReport_results;
	}
	public boolean isCancelReport_search() {
		return isCancelReport_search;
	}
	public void setCancelReport_search(boolean isCancelReport_search) {
		this.isCancelReport_search = isCancelReport_search;
	}
	public boolean isCancelReport_results() {
		return isCancelReport_results;
	}
	public void setCancelReport_results(boolean isCancelReport_results) {
		this.isCancelReport_results = isCancelReport_results;
	}
	public boolean isBookingMod_search() {
		return isBookingMod_search;
	}
	public void setBookingMod_search(boolean isBookingMod_search) {
		this.isBookingMod_search = isBookingMod_search;
	}
	public boolean isBookingMod_results() {
		return isBookingMod_results;
	}
	public void setBookingMod_results(boolean isBookingMod_results) {
		this.isBookingMod_results = isBookingMod_results;
	}
	public boolean isAutoCan_search() {
		return isAutoCan_search;
	}
	public void setAutoCan_search(boolean isAutoCan_search) {
		this.isAutoCan_search = isAutoCan_search;
	}
	public boolean isAutoCan_results() {
		return isAutoCan_results;
	}
	public void setAutoCan_results(boolean isAutoCan_results) {
		this.isAutoCan_results = isAutoCan_results;
	}
	public boolean isAirPass_search() {
		return isAirPass_search;
	}
	public void setAirPass_search(boolean isAirPass_search) {
		this.isAirPass_search = isAirPass_search;
	}
	public boolean isAirPass_results() {
		return isAirPass_results;
	}
	public void setAirPass_results(boolean isAirPass_results) {
		this.isAirPass_results = isAirPass_results;
	}
	public boolean isQuotationReport_search() {
		return isQuotationReport_search;
	}
	public void setQuotationReport_search(boolean isQuotationReport_search) {
		this.isQuotationReport_search = isQuotationReport_search;
	}
	public boolean isQuotationReport_results() {
		return isQuotationReport_results;
	}
	public void setQuotationReport_results(boolean isQuotationReport_results) {
		this.isQuotationReport_results = isQuotationReport_results;
	}
	public boolean isBookingConfirmReport_search() {
		return isBookingConfirmReport_search;
	}
	public void setBookingConfirmReport_search(boolean isBookingConfirmReport_search) {
		this.isBookingConfirmReport_search = isBookingConfirmReport_search;
	}
	public boolean isBookingConfirmReport_results() {
		return isBookingConfirmReport_results;
	}
	public void setBookingConfirmReport_results(
			boolean isBookingConfirmReport_results) {
		this.isBookingConfirmReport_results = isBookingConfirmReport_results;
	}
	public boolean isBookingListReport_search() {
		return isBookingListReport_search;
	}
	public void setBookingListReport_search(boolean isBookingListReport_search) {
		this.isBookingListReport_search = isBookingListReport_search;
	}
	public boolean isBookingListReport_results() {
		return isBookingListReport_results;
	}
	public void setBookingListReport_results(boolean isBookingListReport_results) {
		this.isBookingListReport_results = isBookingListReport_results;
	}
	public boolean isPageLoaded() {
		return isPageLoaded;
	}
	public void setPageLoaded(boolean isPageLoaded) {
		this.isPageLoaded = isPageLoaded;
	}
	public boolean isLoginSuccess() {
		return isLoginSuccess;
	}
	public void setLoginSuccess(boolean isLoginSuccess) {
		this.isLoginSuccess = isLoginSuccess;
	}

}
