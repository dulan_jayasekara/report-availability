package report.test;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import report.controller.PG_Properties;
import report.model.ReportDetails;
import report.reader.CCReportsFlow;
import report.reader.ReportsReader;


public class RunTest {
	
	private WebDriver driver         = null;
	private FirefoxProfile profile;
	
	
	@Before
	public void setUp(){
		
		profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
		driver 			= new FirefoxDriver(profile);
		
	}
	
	@Test
	public void  runTest() throws IOException, InterruptedException{
		
		CCReportsFlow ccReportFlow = new CCReportsFlow(driver);
		ccReportFlow.getOperationsReports();
		ccReportFlow.getFinanceReports();
		ccReportFlow.getMarketingReports();
		ReportDetails reportDetails = ccReportFlow.getContractingReports();
		
		ReportsReader rReader = new ReportsReader(reportDetails);
		rReader.getReports();
	
	}
	
	@After
	public void tearDown() throws IOException{
		
	    driver.quit();
	}
	
	

}
